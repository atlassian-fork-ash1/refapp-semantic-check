package com.atlassian.refapp.sc;

import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Semantic check for verifying UI elements on Refapp welcome page.
 */
public class RefappUISemanticCheckTest {
    private String url = "http://localhost:5990/refapp";
    private WebClient webClient;

    @Before
    public void before(){
        webClient = new WebClient();
        webClient.getOptions().setThrowExceptionOnScriptError(false);
        webClient.getOptions().setUseInsecureSSL(true);
        if(System.getenv("SERVICE_URL") != null)
            this.url = System.getenv("SERVICE_URL");
        System.out.println("RefappUISemanticCheckTest - Refapp base url is "+url);
    }
    
    @Test
    public void checkHomePageElements() throws Exception {
        HtmlPage page = webClient.getPage(url);
        Assert.assertEquals("Welcome to the Atlassian RefApp!", page.getTitleText());
        Assert.assertTrue(page.getElementById("login").isDisplayed());
        Assert.assertTrue(page.asText().contains("This application has no useful features whatsoever"));
        webClient.close();
    }

    @After
    public void after(){
        webClient.close();
    }
}
