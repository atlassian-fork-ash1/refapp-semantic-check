package com.atlassian.refapp.sc;


import com.atlassian.refapp.provisioning.test.plugin.rest.AllTenantData;
import com.atlassian.refapp.provisioning.test.plugin.rest.TenantData;
import com.atlassian.refapp.sc.ssl.InsecureHostnameVerifier;
import com.atlassian.refapp.sc.ssl.InsecureTrustManager;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.client.urlconnection.HTTPSProperties;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.net.ssl.SSLContext;
import javax.ws.rs.core.MediaType;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Semantic check for verifying insertion and retrieval from tenant database.
 */
public class RefappAPISemanticCheckTest {
    private String url = "http://localhost:5990/refapp";
    private static final String INSERT = "/rest/prov/1.0/insert/";
    private static final String SELECT = "/rest/prov/1.0/select";

    @Before
    public void before() {
        if (System.getenv("SERVICE_URL") != null)
            this.url = System.getenv("SERVICE_URL");
        System.out.println("RefappAPISemanticCheckTest - Refapp base url is " + url);
    }

    @Test
    public void verifyInsertAndSelectFromTenantDatabase() throws Exception {
        final String data = retrieveTenantID(url) + "_" + System.nanoTime();
        Assert.assertEquals(200, get(url + INSERT + data).getStatus());

        ClientResponse response = get(url + SELECT);
        Assert.assertEquals(200, response.getStatus());

        // Deserialization with Jersey is failing because of Media type error, so a hack with GSon.
        // Replace this once the problem is resolved.
        Gson gson = new GsonBuilder().create();
        AllTenantData allTenantData =
                gson.fromJson(response.getEntity(String.class),AllTenantData.class);

        boolean foundData = false;
        for (TenantData tenantData : allTenantData.getTenantDatas()) {
            if (!retrieveTenantID(url).equals(tenantData.getTenant())) {
                Assert.fail("some other tenant's data is in the database: '" + tenantData.getTenant() + "'");
            }

            if (data.equals(tenantData.getData())) {
                foundData = true;
            }
        }
        Assert.assertTrue("Didn't find the data we just inserted '" + data + "'", foundData);
    }

    private ClientResponse get(String url) throws Exception{
        ClientConfig config = new DefaultClientConfig();
        SSLContext ctx = SSLContext.getInstance("SSL");
        InsecureTrustManager manager[] = new InsecureTrustManager[]{ new InsecureTrustManager()};
        ctx.init(null, manager, null);
        config.getProperties().put(HTTPSProperties.PROPERTY_HTTPS_PROPERTIES,
                new HTTPSProperties(new InsecureHostnameVerifier(), ctx));
        final Client client = Client.create(config);
        final WebResource webResource = client.resource(url);
        return webResource.accept(MediaType.APPLICATION_JSON_TYPE).get(ClientResponse.class);
    }

    private String retrieveTenantID(String url) throws MalformedURLException {
        URL tUrl = new URL(url);
        return tUrl.getHost().split("\\.")[0];
    }
}